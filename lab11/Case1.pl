% dropAny(?Elem,?List,?OutList)
dropAny(X,[X|T],T).
dropAny(X,[H|Xs],[H|L]):-dropAny(X,Xs,L).

%Ex1.1 dropFirst: drops only the first occurrence (showing no alternative results)
dropFirst(X,[X|T],T):-!.
dropFirst(X, [H|Xs],[H|L]):-dropFirst(X,Xs,L).

%Ex1.2 dropLast: drops only the last occurrence (showing no alternative results)
dropLast(X,[H|Xs],[H|L]):-dropLast(X,Xs,L),!.
dropLast(X,[X|T],T).

%Ex1.3 dropAll: drop all occurrences, returning a single list as result
dropAll(X, [], []) :- !.
dropAll(X,[X|T],L):-!,dropAll(X,T,L).
dropAll(X,[H|Xs],[H|L]):-dropAll(X,Xs,L).