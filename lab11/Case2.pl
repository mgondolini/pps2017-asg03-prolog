% Ex2.1 fromList(+List,-Graph)
fromList([_],[]).
fromList([H1,H2|T],[e(H1,H2)|L]):- fromList([H2|T],L).

%Ex2.2 fromCircList(+List,-Graph)
fromCircList([],[]).
fromCircList([H|T],G):- append([H|T],[H],L), fromList(L,G).

% Ex2.3 dropNode(+Graph, +Node, -OutGraph)
% drop all edges starting and leaving from a Node use dropAll defined in 1.1
dropAll(X, [], []) :- !.
dropAll(X,[X|T],L):-!,dropAll(X,T,L).
dropAll(X,[H|Xs],[H|L]):-dropAll(X,Xs,L).

dropNode(G,N,O):- dropAll(e(N,_),G,G2), dropAll(e(N,_),G2,O). % toglie il nodo che inizia per N

% Ex 2.4 reaching(+Graph, +Node, -List)
% all the nodes that can be reached in 1 step from Node
% possibly use findall, looking for e(Node,_) combined with member(?Elem,?List)
reaching([], N, []):-!.
reaching(G, N, L):-findall(X, member(e(N,X), G), L).

% Ex2.5 anypath(+Graph, +Node1, +Node2, -ListPath)
% a path from Node1 to Node2
% if there are many path, they are showed 1-by-1
%� a path from N1 to N2 exists if there is a e(N1,N2)
%� a path from N1 to N2 is OK if N3 can be reached from N1
%anypath([e(1,2),e(1,3),e(2,3)],1,3,L).
%and then there is a path from N2 to N3, recursively						
anypath(G, N1, N2, [e(N1, N2)]) :- member(e(N1, N2), G).
anypath(G, N1, N2, [e(N1, N3)|L]) :- member(e(N1, N), G), anypath(G, N3, N2, L).
															 

% Ex 2.6 allreaching(+Graph, +Node, -List)
% all the nodes that can be reached from Node
% Suppose the graph is NOT circular!
% Use findall and anyPath!
allreaching(G, N, L) :- findall(N1, anypath(G, N, N1, L1), L).


