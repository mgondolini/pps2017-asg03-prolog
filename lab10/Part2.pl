% Ex2.1 size(List,Size)
% Size will contain the number of elements in List
size([],0).
size([_|T],M) :- size(T,N), M is N+1.

% Ex2.2 size(List,Size)
% Size will contain the number of elements in List,
% written using notation zero, s(zero), s(s(zero))..
size2([],zero).
size2([_|T],s(N)) :- size2(T,N).

% Ex2.3 sum(List,Sum)
sum([],0).
sum([H|T],SUM) :- sum(T, S), SUM is H+S.

% Ex2.4 average(List,Average)
% it uses average(List,Count,Sum,Average)
average(List,A) :- average(List,0,0,A).
average([],C,S,A) :- A is S/C.
average([X|Xs],C,S,A) :-C2 is C+1,S2 is S+X,average(Xs,C2,S2,A).

% Ex2.5 max(List,Max)
% Max is the biggest element in List
% Suppose the list has at least one element
%� first develop: max(List,Max,TempMax)
%� where TempMax is the maximum found so far (initially it is the first number in the list.)

max([X|Xs], M) :- max(Xs, M, X).
max([], M, M). 
max([X|Xs], M, TM) :- X =< TM, max(Xs, M, TM).
max([X|Xs], M, TM) :- X > TM, max(Xs, M, X). 
