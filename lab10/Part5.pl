% inv(List,List)
% example: inv([1,2,3],[3,2,1]).
inv(L1,L2):-inv(L1,[],L2).
inv([], L2, L2).
inv([H|T],X,L2):-inv(T,[H|X],L2).

% double(List,List)
% suggestion: remember predicate append/3
% example: double([1,2,3],[1,2,3,1,2,3]).
double(L1,L2) :- append(L1,L1,L2).

% times(List,N,List)
% example: times([1,2,3],3,[1,2,3,1,2,3,1,2,3]).
times(L1,N,L2):-times(L1,N,0,[],L2).
times(L1,N,N,L2,L2).
times(L1,N,I,LAcc,L2):- append(L1,LAcc,LAcc2), M is I+1, M =< N, times(L1,N,M,LAcc2,L2).

% proj(List,List)
% example: proj([[1,2],[3,4],[5,6]],[1,3,5]).
proj([],[]).
proj([[H|T]| Rest1],[H|Rest2]):- proj(Rest1,Rest2).