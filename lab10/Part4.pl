% Ex4.1 seq(N,List)
% example: seq(5,[0,0,0,0,0]).
seq(0,[]).
seq(N,[0|T]):- N > 0, N2 is N-1, seq(N2,T).

% Ex4.2 seqR(N,List)
% example: seqR(4,[4,3,2,1,0]).
seqR(0,[0]).
seqR(N,[H|T]):- N > 0, H is N, N2 is N-1, seqR(N2,T).

% Ex4.3 seqR2(N,List)
% example: seqR2(4,[0,1,2,3,4]).
seqR2(N,List) :- seqR2(N,List,0). %I=0
seqR2(0,[I],I). %I numero da incrementare
seqR2(N,[I|T],I):- N > 0, N2 is N-1, I2 is I+1, seqR2(N2,T,I2).
